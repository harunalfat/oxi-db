FacebookLoginButton = React.createClass({
  loginClick(){
    Meteor.loginWithFacebook({}, function(err){
      if (err){
        throw new Meteor.Error("Facebook Login Failed");
      }
    });
  },
  render(){
    return (
      <a href="#" className="pure-button pure-button-primary" onClick={this.loginClick}>
        Login with Facebook
      </a>
    );
  }
});
