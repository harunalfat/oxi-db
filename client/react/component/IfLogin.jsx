IfLogin = React.createClass({
  mixins: [ReactMeteorData],

  getMeteorData(){
    return{
      currentUser: Meteor.user()
    };
  },
  render(){
    if (this.data.currentUser){
      return <Layout/>;
    }
    else{
      return <Landing/>;
    }
  }
});
