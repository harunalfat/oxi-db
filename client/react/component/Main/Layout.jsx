Layout = React.createClass({
  mixins: [ReactMeteorData],

  getMeteorData(){
    return {
      currentUser: Meteor.user()
    };
  },

  logoutClick(){
    Meteor.logout(function(err){
      if (err){
        throw new Meteor.error("Logout failed");
      }
    });
  },

  menuClick(e){
    e.preventDefault();
    toggleClass(layout, active);
    toggleClass(menu, active);
    toggleClass(menuLink, active);
  },

  render(){
    return(
      <div id="layout">
        <a href="#menu" id="menuLink" className="menu-link" onClick={this.menuClick}>
            <span></span>
        </a>
        <div id="menu">
            <div className="pure-menu">
                <a className="pure-menu-heading" href="#">
                  Company
                </a>

                <ul className="pure-menu-list">
                    <li className="pure-menu-item"><a href="#" className="pure-menu-link">{this.data.currentUser.services.facebook.name}</a></li>
                    <li className="pure-menu-item"><a href="#" className="pure-menu-link">About</a></li>
                    <li className="pure-menu-item"><a href="#" className="pure-menu-link">Services</a></li>
                    <li className="pure-menu-item"><button className="pure-button pure-button-primary" onClick={this.logoutClick}>Logout</button></li>
                </ul>
            </div>
        </div>
        <MainTab/>
        <script src="js/ui.js"></script>
      </div>
    );
  }
});
